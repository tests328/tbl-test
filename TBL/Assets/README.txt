Author: Mateusz Moczadło
Git: https://gitlab.com/yeronimo-apps/tbl-test.git

Assets: example asset package 
Work time: 
	24.01 - 19.30-22.30 3h (30 min git and project set)
	25.01 - 17.30-21.00 3.5h (20-21 bug fixing, finish tweaks)
	total: 6.5h

In almost 6h of work all objectives of the task have been achieved.