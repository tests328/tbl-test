using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
    [SerializeField] private float scrollSpeed = -.5f;

    private Material material;
    private IGameController gameController;
    private bool keepScrolling;

    void Awake()
    {
        material = GetComponent<Renderer>().material;
        gameController = GetComponentInParent<IGameController>();
        gameController.GameStateChanged += GameController_GameStateChanged;
    }

    void Update()
    {
        if (keepScrolling) Scroll();
    }

    private void Scroll()
    {
        material.mainTextureOffset += new Vector2(Time.deltaTime * scrollSpeed, 0f);
    }

    private void GameController_GameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.StartPlaying: keepScrolling = true;
                break;

            case GameState.StopPlaying: keepScrolling = false;
                break;
        }
    }
}