using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Globalization;

public class HUDUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI lifesText;
    [SerializeField] private TextMeshProUGUI timeText;

    public void UpateScore(int score)
    {
        scoreText.text = score.ToString();
    }

    public void UpateTime(float time)
    {
        timeText.text = time.ToString("F2");
    }

    public void UpateLifes(int lifes)
    {
        lifesText.text = lifes.ToString();
    }
}
