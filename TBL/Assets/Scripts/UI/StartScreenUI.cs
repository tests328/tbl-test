using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartScreenUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI bestScore;
    [SerializeField] private TextMeshProUGUI lastScore;

    private void OnEnable()
    {
        UpdateBestScore();
    }

    void Start()
    {
        UpdateLastScore(-1);
    }

    private void UpdateBestScore()
    {
        bestScore.text = $"{PlayerPrefs.GetInt("bestScore", 0)}";
    }

    public void UpdateLastScore(int score)
    {
        if (score < 0)
            lastScore.text = "-";
        else
            lastScore.text = score.ToString();
    }
}