using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameStatsData", menuName = "Data/GameStatsData")]
public class GameStatsData : ScriptableObject
{
    public int Lifes;
    public int Score;
}