using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : MonoBehaviour
{
    public List<T> Objects => objectPool;

    private List<T> objectPool;

    private Transform parent;
    private T objectPrefab;

    public ObjectPool(T prefab, int initialQuantity, Transform parent = null)
    {
        objectPrefab = prefab;
        this.parent = parent;
        objectPool = new List<T>();
        for (int i = 0; i < initialQuantity; i++)
        {
            SpawnObject();
        }
    }

    public T GetObject()
    {
        T gO = null;

        foreach (var item in objectPool)
        {
            if (!item.gameObject.activeInHierarchy)
            {
                gO = item;
                break;
            }
        }

        if (gO is null)
        {
            gO = SpawnObject();
        }
        return gO;
    }

    private T SpawnObject()
    {
        var newObject = UnityEngine.Object.Instantiate(objectPrefab, parent);
        newObject.gameObject.SetActive(false);
        objectPool.Add(newObject);
        return newObject;
    }
}
