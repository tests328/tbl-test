using System;

public interface IScoreHandler
{
    event Action<int> ScoreUpdated;

    void AddPoints(int points);
    int LastScore { get; }
}