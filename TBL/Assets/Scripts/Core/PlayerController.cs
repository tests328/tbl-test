using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public event Action Hit;

    List<Collider2D> hitColliders;

    private void Awake()
    {
        hitColliders = new List<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && !hitColliders.Contains(collision))
        {
            hitColliders.Add(collision);
            Hit?.Invoke();
        }
    }

    private void OnDisable()
    {
        hitColliders.Clear();
    }
}