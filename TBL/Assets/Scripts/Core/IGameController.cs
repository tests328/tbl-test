using System;

public interface IGameController
{
    event Action<GameState> GameStateChanged;
}