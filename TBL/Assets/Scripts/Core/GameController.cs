using System;
using UnityEngine;

public class GameController : MonoBehaviour, IGameController
{
    public event Action<GameState> GameStateChanged;

    [SerializeField] private int lifes = 3;
    [SerializeField] private float gameTime = 60f;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private HUDUI hud;  
    [SerializeField] private StartScreenUI startScreen;  
 
    private GameState currentGameState = GameState.StopPlaying;
    private int currentLifes = 0;
    private IScoreHandler scoreHandler;
    private float gameTimer = 0f;

    private void Awake()
    {
        currentLifes = lifes;
        if (playerController) playerController.Hit += PlayerController_Hit;
        scoreHandler = GetComponent<ScoreHandler>();
        scoreHandler.ScoreUpdated += ScoreHandler_ScoreUpdated;
    }

    private void Update()
    {
        ListenOnGameStartInput();
        GameTimer();
    }

    private void ScoreHandler_ScoreUpdated(int score)
    {
        hud.UpateScore(score);
    }

    private void PlayerController_Hit()
    {
        currentLifes--;
        hud.UpateLifes(currentLifes);
        if (currentLifes == 0) StopGame();
    }

    private void ListenOnGameStartInput()
    {
        if (currentGameState == GameState.StartPlaying) return;

        if (Input.anyKey) StartGame();
    }

    private void StartGame()
    {
        currentGameState = GameState.StartPlaying;
        gameTimer = gameTime;
        hud.UpateLifes(currentLifes);
        hud.UpateTime(gameTimer);
        GameStateChanged?.Invoke(currentGameState);
        playerController.gameObject.SetActive(true);
        startScreen.gameObject.SetActive(false);
    }

    private void StopGame()
    {
        currentGameState = GameState.StopPlaying;
        GameStateChanged?.Invoke(currentGameState);
        currentLifes = lifes;
        playerController.gameObject.SetActive(false);
        hud.UpateLifes(currentLifes);
        startScreen.UpdateLastScore(scoreHandler.LastScore);
        startScreen.gameObject.SetActive(true);
    }

    private void GameTimer()
    {
        if (currentGameState == GameState.StopPlaying) return;

        gameTimer -= Time.deltaTime;
        if (gameTimer <= 0)
        {
            StopGame();
            gameTimer = 0;
        }
        hud.UpateTime(gameTimer);
    }
}