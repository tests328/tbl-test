using System;
using UnityEngine;

public interface IPoolable
{
    event Action<IPoolable> Reset;

    void SetPoolable(Vector3 positon);
    void ResetPoolable();
    void SetParent(Transform parent);
}