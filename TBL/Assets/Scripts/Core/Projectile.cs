using System;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour, IPoolable
{
    [SerializeField] private float movementSpeed = 5f;
    private Vector3 moveVector;

    public event Action<IPoolable> Reset;

    void Update()
    {
        if (moveVector != Vector3.zero) Move();
    }

    protected virtual void Move()
    {
        transform.Translate(Time.deltaTime * moveVector);
    }

    public void SetPoolable(Vector3 positon)
    {
        transform.position = positon;
        gameObject.SetActive(true);
        moveVector = new Vector3(movementSpeed, 0f, 0f);
    }

    public void ResetPoolable()
    {
        moveVector = Vector3.zero;
        gameObject.SetActive(false);
        Reset?.Invoke(this);
    }


    protected virtual void Destroy()
    {
        ResetPoolable();
    }

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out IDestroyable destroyable))
        {
            destroyable.Destroy();
            Destroy();
        }
    }
}