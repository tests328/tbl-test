using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField] private float movementSpeed = 5f;
    [SerializeField] private float verticalMovementBoundary;

    private Vector2 verticalBounds;
    private void Awake()
    {
        CalculateMovementBounds(Camera.main);
    }

    private void Update()
    {
        ReadMovementInput();
    }

    private void ReadMovementInput()
    {
        float inputY = Input.GetAxis("Vertical");

        Vector2 moveVector = new Vector2(0f, inputY);
        if (moveVector != Vector2.zero)
            Move(moveVector);
    }

    private void Move(Vector3 move)
    {
        Vector3 newPos = transform.position + move;
        newPos.y = Mathf.Clamp(newPos.y, verticalBounds.x + verticalMovementBoundary, verticalBounds.y - verticalMovementBoundary);

        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * movementSpeed);
    }

    private void CalculateMovementBounds(Camera camera)
    {
        Vector3 upperLeft = camera.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        Vector3 lowerRight = camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));

        verticalBounds = new Vector2(lowerRight.y, upperLeft.y);
    }
}
