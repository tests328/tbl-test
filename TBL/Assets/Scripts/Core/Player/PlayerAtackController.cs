using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAtackController : MonoBehaviour
{
    [SerializeField] private ProjectileSpawner projectileSpawner;
    [SerializeField] private float atackCooldown = 1f;

    private float nextAtack = 0f;
    private float atackTimer = 0f;
    private bool listenInput;

    private void OnEnable()
    {
        listenInput = true;
        nextAtack = 0f;
    }

    private void Update()
    {
        if (listenInput) ReadAtackInput();
    }

    private void ReadAtackInput()
    {
        atackTimer += Time.deltaTime;
        if (Input.GetKey(KeyCode.Space) && atackTimer > nextAtack)
        {         
           atackTimer = 0;
           nextAtack = atackCooldown;
           projectileSpawner?.SpawnProjectile();
        }
    }

    private void OnDisable()
    {
        listenInput = false;
        atackTimer = 0f;
    }
}