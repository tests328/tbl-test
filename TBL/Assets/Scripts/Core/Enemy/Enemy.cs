using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IPoolable, IDestroyable
{
    [SerializeField] private float movementSpeed = -5f;
    [SerializeField] private int points = 1;

    private Vector3 moveVector;

    public event Action<IPoolable> Reset;

    private IScoreHandler scoreHandler;
    private bool destroyed;

    private void Awake()
    {
        scoreHandler = GetComponentInParent<IScoreHandler>();
    }

    void Update()
    {
        if (moveVector != Vector3.zero) Move();
    }

    protected virtual void Move()
    {
        transform.Translate(Time.deltaTime * moveVector);
    }

    protected virtual void StartMove()
    {
        moveVector = new Vector3(movementSpeed, 0f, 0f);
    }

    protected virtual void StopMove()
    {
        moveVector = Vector3.zero;
    }

    public virtual void SetPoolable(Vector3 positon)
    {
        transform.position = positon;
        gameObject.SetActive(true);
        StartMove();
    }

    public virtual void ResetPoolable()
    {
        StopMove();
        gameObject.SetActive(false);       
    }

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public void Destroy()
    {
        ResetPoolable();
        scoreHandler?.AddPoints(points);
    }
}