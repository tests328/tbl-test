using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public struct SpawnRange
{
    public int Min => min;
    public int Max => max;

    [SerializeField, Range(0,5)] private int min;
    [SerializeField, Range(0,5)] private int max;
}

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<Transform> spawnPoints;
    [SerializeField] private List<Enemy> enemies;
    [SerializeField] private SpawnRange spawnRangeAmount;
    [SerializeField] private float spawnCooldown;
    [SerializeField] private float delayBetweenSpawn;

    private IGameController gameController;

    private List<ObjectPool<Enemy>> enemiesPool;

    void Start()
    {
        InitializeEnemiesPool();

        gameController = GetComponentInParent<IGameController>();
        gameController.GameStateChanged += GameController_GameStateChanged;
    }

    private void InitializeEnemiesPool()
    {
        enemiesPool = new List<ObjectPool<Enemy>>();
        for (int i = 0; i < enemies.Count; i++)
        {
            enemiesPool.Add(new ObjectPool<Enemy>(enemies[i], 5, transform));
        }
    }

    private void GameController_GameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.StartPlaying:StartSpawnEnemies();
                break;

            case GameState.StopPlaying: StopSpawnEnemies();
                break;
        }
    }

    private void StartSpawnEnemies()
    {
        InvokeRepeating(nameof(SpawnEnemies), 0f, spawnCooldown);
    }

    private void StopSpawnEnemies()
    {
        CancelInvoke(nameof(SpawnEnemies));
        foreach (var pool in enemiesPool)
        {
            foreach (var item in pool.Objects)
            {
                item.ResetPoolable();
            }
        }
    }

    private void SpawnEnemies()
    {       
        int enemiesCount = Random.Range(spawnRangeAmount.Min, spawnRangeAmount.Max);
        List<Transform> spawnTransforms = new List<Transform>(spawnPoints);
        for (int i = 0; i < enemiesCount; i++)
        {
            Enemy enemyToSpawn = enemiesPool[Random.Range(0, enemiesPool.Count)].GetObject();
            int randomTransformIndex = Random.Range(0, spawnTransforms.Count);
            SpawnEnemy(spawnTransforms[randomTransformIndex].position, enemyToSpawn);
            spawnTransforms.RemoveAt(randomTransformIndex);
        }
    }

    private void SpawnEnemy(Vector3 postion, Enemy enemy)
    {
        enemy.SetPoolable(postion);
    }

    private void OnDestroy()
    {
        if (gameController != null) gameController.GameStateChanged += GameController_GameStateChanged;
    }
}