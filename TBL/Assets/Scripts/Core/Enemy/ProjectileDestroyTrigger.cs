using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDestroyTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Projectile projectile))
        {
            projectile.ResetPoolable();
        }
    }
}