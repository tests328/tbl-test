using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreHandler : MonoBehaviour, IScoreHandler
{
    public event Action<int> ScoreUpdated;
    public int LastScore => lastScore;

    private IGameController gameController;
    private int score = 0;
    private int lastScore = 0;


    void Start()
    {
        gameController = GetComponent<IGameController>();
        gameController.GameStateChanged += GameController_GameStateChanged;
    }

    private void GameController_GameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.StartPlaying:
                ResetScore();
                break;

            case GameState.StopPlaying:
                CalculacteBestScore(score);
                break;
        }
    }

    public void AddPoints(int points)
    {
        score += points;
        ScoreUpdated?.Invoke(score);
    }

    private void ResetScore()
    {
        score = 0;
        lastScore = score;
        ScoreUpdated?.Invoke(score);
    }

    private void CalculacteBestScore(int score)
    {
        lastScore = score;
        int bestScore = PlayerPrefs.GetInt("bestScore", 0);
        if (score <= bestScore) return;

        PlayerPrefs.SetInt("bestScore", score);
        PlayerPrefs.Save();
    }
}
