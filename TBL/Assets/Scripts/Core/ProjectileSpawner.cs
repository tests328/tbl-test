using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
    [SerializeField] private List<Transform> projectileSpawns;
    [SerializeField] private Projectile projectilePrefab;

    private ObjectPool<Projectile> projectilePool;

    private void Awake()
    {
        projectilePool = new ObjectPool<Projectile>(projectilePrefab, 10, transform);
        foreach (var poolable in projectilePool.Objects)
        {
            poolable.Reset += OnPoolableReset;
        }
    }

    private void OnPoolableReset(IPoolable poolable)
    {
        poolable.SetParent(transform);
    }

    public void SpawnProjectile()
    {
        foreach (var item in projectileSpawns)
        {
            Projectile projectile = projectilePool.GetObject();
            projectile.SetParent(null);
            projectile.SetPoolable(item.position);
        }
    }

    private void OnApplicationQuit()
    {
        foreach (var poolable in projectilePool.Objects)
        {
            poolable.SetParent(transform);
        }
    }

    private void OnDisable()
    {
        foreach (var poolable in projectilePool.Objects)
        {
            poolable.Reset -= OnPoolableReset;
            poolable.ResetPoolable();
        }
    }
}